delimiter //

create procedure MAXRANGE()
begin

create temporary table dateRange
select DISTINCT `Date`, ABS(Open-Close) as Range1 from sd3
order by Range1 desc limit 3;
Select * from dateRange;


create temporary table maxhigh
Select Date, max(High) as maxHigh from sd3
where Date in (SELECT Date from dateRange)
group by Date
order by 2 desc limit 3;
Select * from maxhigh;


create temporary table timeofmaxhigh
Select dateRange.Date, Time from sd3
JOIN maxhigh dateRange ON sd3.Date=dateRange.Date
where maxHigh=High;
SELECT * from timeofmaxhigh;


SELECT dateRange.Date, Round(Range1,4) as `Range`, timeofmaxhigh.Time as `Time of Max Price`
from dateRange JOIN timeofmaxhigh ON dateRange.Date=timeofmaxhigh.Date
order by Range1 desc;
 


drop table maxhigh;
drop table dateRange;
drop table timeofmaxhigh;

end //

delimiter ;

call MAXRANGE()
