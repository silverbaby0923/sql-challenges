#Stored Procedure for calculating VWAP#   
delimiter //

create procedure VWAP(startingdate varchar(12))
begin   
     set @sdate=startingdate;
	 select @edate:=
     DATE_FORMAT(DATE_ADD(
     str_to_date(
     @sdate, "%Y%m%d%H%i"), INTERVAL 5 HOUR), "%Y%m%d%H%i");
     select @etime:=
     DATE_FORMAT(DATE_ADD(
     str_to_date(
     @sdate, "%Y%m%d%H%i"), INTERVAL 5 HOUR), "%H:%i");
     
     SELECT SUM(vol*close)/SUM(vol) as VWAP,
     DATE_FORMAT(str_to_date(@sdate,"%Y%m%d%H%i"), "%d/%m/%Y") as Date,
     DATE_FORMAT(str_to_date(@sdate,"%Y%m%d%H%i"), "%H:%i") as Start,
     @etime as `End` from sd2
     WHERE date BETWEEN @sdate AND @edate;

end //

delimiter ;

#Drop the procedure if no longer needed#
-- drop procedure VWAP;

