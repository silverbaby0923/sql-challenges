import re
original=open("opra_example_regression.log","r")
record=re.compile('Record Publish:')
type=re.compile('Type: Trade')
price=re.compile('wTradePrice')
volume=re.compile('wTradeVolume')

rec=ty=pr=vol=""
for line in original:
    if record.search(line):
        rec=line

    if type.search(line):
        ty=line

    if rec and ty:
        if price.search(line):
            pr=line
        if volume.search(line):
            vol=line
        if rec and ty and pr and vol:
            line=rec.strip('Regression:').lstrip().rstrip()+ty.strip('Regression:')+pr.strip('Regression:')+vol.strip('Regression:')
            print(line)
            rec=ty=pr=vol=""
