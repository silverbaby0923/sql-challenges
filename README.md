SQL Challenge 1

## Assumptions
1. Used Close price as the Trade price to calculate Volume-Weighted Average Price

2. The dataset used is named as sd2 in MYSQL


## SQL used to change column headings

ALTER TABLE `sd2` 
   CHANGE COLUMN `<ticker>` `ticker` TEXT,
   CHANGE COLUMN `<date>` `date` TEXT,
   CHANGE COLUMN `<open>` `open` TEXT,
   CHANGE COLUMN `<high>` `high` TEXT,
   CHANGE COLUMN `<low>` `low` TEXT,
   CHANGE COLUMN `<close>` `close` TEXT,
   CHANGE COLUMN `<vol>` `vol` TEXT

## Script file to be used
challenge_1.sql



## Command to run the script
call VWAP('yyyymmddhhmm')      e.g.call VWAP('201010110930')

###########################################################################################

SQL Challenge 2

##Assumptions
1. The dataset used is named as sd3 in MySQL

##Script file to be used
challenge_2.sql

## Command to run the script
call MAXRANGE()

############################################################################################

Python Challenge 1

## Description
1. Two new files will be created when running this script, 'IPaddress' and 'new.real' 

2. Open 'IPaddress' to verify that the comment line has been removed

3. MAC addresses will be printed on the screen when running the script

3. IP addresses are stored in the file 'new.real'


## Script file to be used
challenge_1.py



## Command to run the script
python challenge_1.py

#############################################################################################


Python Challenge 2

## Description
1. The required output will be printed on the screen when running the script



## Script file to be used
challenge_2.py



## Command to run the script
python challenge_2.py

##############################################################################################
